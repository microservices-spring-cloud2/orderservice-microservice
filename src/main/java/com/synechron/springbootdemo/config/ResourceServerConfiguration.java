package com.synechron.springbootdemo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

	@Override
	public void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
			/*.antMatchers(HttpMethod.GET, "/api/v1/orders/**")
				.hasAnyRole("USER", "ADMIN")
			.antMatchers(HttpMethod.POST, "/api/v1/orders/**")
				.hasAnyRole("ADMIN")
			.antMatchers(HttpMethod.DELETE, "/api/v1/orders/**")
				.hasAnyRole("ADMIN")
			*/.anyRequest()
				.permitAll();
	}

}
