package com.synechron.springbootdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode(of="id")
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class Payment {

	private long id;
	
	private double money;
	
	private String from;
	
	private String to;
	
	private String notes;
	
	private String status;
}
