package com.synechron.springbootdemo.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import com.synechron.springbootdemo.model.Payment;

@FeignClient("PAYMENTSERVICE")
public interface PaymentFeignClient {
	
	@PostMapping("/api/v1/payment")
	public ResponseEntity<Payment> acceptPayment(Payment payment);
}
