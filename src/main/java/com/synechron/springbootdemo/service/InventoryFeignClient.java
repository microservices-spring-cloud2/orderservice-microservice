package com.synechron.springbootdemo.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient("INVENTORYSERVICE")
public interface InventoryFeignClient {
	
	 @PostMapping("/api/v1/inventory/")
	 public ResponseEntity<Integer> updateQty();

	 @GetMapping(value = "/api/v1/inventory")
	 public int fetchItemsCount();
}
