package com.synechron.springbootdemo.service;

import com.synechron.springbootdemo.dao.OrderRepository;
import com.synechron.springbootdemo.model.Order;
import com.synechron.springbootdemo.model.Payment;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
@AllArgsConstructor
@Slf4j
public class OrderServiceImpl implements OrderService {

	private OrderRepository orderDAO;

	private DiscoveryClient discoveryClient;

	private RestTemplate restTemplate;
	
	private InventoryFeignClient feignClient;
	
	private PaymentFeignClient paymentFeignClient;
	
	private final Source source;

	@Override
	//@CircuitBreaker(name = "inventoryservice", fallbackMethod="fallBack")
	@Retry(name="orderserviceRetry", fallbackMethod="fallBack")
	public Order saveOrder(Order order) {
		// call the inventory service
		// make the post request
		// 1st approach
		log.info("Invoking the REST endpoing of Inventory microservice:: ");
		// approach-1
		// discoveryClient();
		// processPayment();
		// approach-2
		loadBalancedUsingRestTempate(order);
		//approach-3
		/*Payment payment = Payment
				.builder()
				.from("Kiran")
				.to("Vinay")
				.id(12)
				.money(25000)
				.notes("purchase")
				.build();
		ResponseEntity<Payment>paymentResponse = this.paymentFeignClient.acceptPayment(payment);
		
		log.info(" Payment response ", paymentResponse.getBody());
		
		this.feignClient.updateQty();
		
		log.info("Fetched the quantity :: " +this.feignClient.fetchItemsCount());*/

		log.info("Successfully Invoked the REST endpoing of Inventory microservice:: ");
		return this.orderDAO.save(order);
	}

	private void loadBalancedUsingRestTempate(Order order) {
		Payment payment = Payment
							.builder()
							.from("Kiran")
							.to("Vinay")
							.id(12)
							.money(25000)
							.notes("purchase")
							.build();
		
		//Object response = this.restTemplate.postForEntity("http://PAYMENTSERVICE/api/v1/payment", payment, Object.class);
		//this.restTemplate.postForEntity("http://INVENTORYSERVICE/api/v1/inventory", null, Integer.class);
		//Object fetchCount = this.restTemplate.getForEntity("http://INVENTORYSERVICE/api/v1/inventory", Integer.class);
		
		//send the order message to inventory topic
		
		this.source.output().send(MessageBuilder.withPayload(order).build());
		
		log.info("sending messages to orders topic" );
		//log.info("Response :: " + ((ResponseEntity<Object>) fetchCount).getBody());

	}

	private void processPayment() {
		List<ServiceInstance> instances = this.discoveryClient.getInstances("PAYMENTSERVICE");

		if (instances != null && !instances.isEmpty()) {
			String uri = instances.get(0).getUri().toString() + "/api/v1/payment";
			log.info("URI :: " + uri);

			Payment payment = Payment.builder().from("Kiran").to("Vinay").id(12).money(25000).notes("purchase").build();

			Object response = this.restTemplate.postForEntity(uri, payment, Object.class);
			log.info("Response :: " + ((ResponseEntity<Object>) response).getBody());
		}

	}

	private void discoveryClient() {
		List<ServiceInstance> instances = this.discoveryClient.getInstances("INVENTORYSERVICE");

		if (instances != null && !instances.isEmpty()) {
			String uri = instances.get(0).getUri().toString() + "/api/v1/inventory";
			log.info("URI :: " + uri);
			Object response = this.restTemplate.postForEntity(uri, null, Object.class);
			log.info("Response :: " + ((ResponseEntity<Integer>) response).getBody());
		}
	}
	
	private Order fallBack(Throwable throwable) {
		log.error("Failed calling the inventorymicroservice::");
		log.error(throwable.getMessage());
		return Order.builder().orderId(1111).orderDate(LocalDate.now()).totalPrice(25000).build();
	}

	@Override
	public Set<Order> fetchOrders() {
		return new HashSet<>(this.orderDAO.findAll());
	}

	@Override
	public Order fetchOrderByOrderId(long orderId) {
		return this.orderDAO.findById(orderId).orElseThrow(OrderServiceImpl::invalidOrderId);
	}

	private static IllegalArgumentException invalidOrderId() {
		return new IllegalArgumentException("Invalid Order Id");
	}

	@Override
	public void archiveOrderByOrderId(long orderId) {
		this.orderDAO.deleteById(orderId);
	}

}